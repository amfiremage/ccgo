# ccgo/v3

Package ccgo translates C to Go source code.

This v3 package is obsolete. Please use current ccgo/v4:

# The command

	https://modernc.org/ccgo/v4

# The library

	https://modernc.org/ccgo/v4/lib
