module modernc.org/ccgo/v3

go 1.20

require (
	github.com/dustin/go-humanize v1.0.1
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/pmezard/go-difflib v1.0.0
	golang.org/x/sys v0.9.0
	golang.org/x/tools v0.10.0
	modernc.org/cc/v3 v3.41.0
	modernc.org/ccgo/v4 v4.0.0-20230827202736-8661c3d9955b
	modernc.org/ccorpus v1.11.6
	modernc.org/libc v1.24.1
	modernc.org/mathutil v1.6.0
	modernc.org/opt v0.1.3
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/mod v0.11.0 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/cc/v4 v4.13.2 // indirect
	modernc.org/gc/v2 v2.3.0 // indirect
	modernc.org/httpfs v1.0.6 // indirect
	modernc.org/memory v1.7.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
