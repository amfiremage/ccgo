module modernc.org/ccgo/v4

go 1.23

require (
	github.com/dustin/go-humanize v1.0.1
	github.com/pbnjay/memory v0.0.0-20210728143218-7b4eea64cf58
	github.com/pmezard/go-difflib v1.0.0
	golang.org/x/mod v0.23.0
	golang.org/x/tools v0.30.0
	modernc.org/cc/v4 v4.25.0
	modernc.org/ccgo/v3 v3.17.0
	modernc.org/ccorpus2 v1.5.2
	modernc.org/fileutil v1.3.0
	modernc.org/gc/v2 v2.6.3
	modernc.org/libc v1.61.13
	modernc.org/mathutil v1.7.1
	modernc.org/opt v0.1.4
	modernc.org/strutil v1.2.1
)

require (
	github.com/google/uuid v1.6.0 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/ncruces/go-strftime v0.1.9 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/exp v0.0.0-20230315142452-642cacee5cc0 // indirect
	golang.org/x/sync v0.11.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/cc/v3 v3.41.0 // indirect
	modernc.org/memory v1.8.2 // indirect
	modernc.org/sortutil v1.2.1 // indirect
	modernc.org/token v1.1.0 // indirect
)
